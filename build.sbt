
lazy val commonSettings = Seq(
  organization := "se.spegelreflex",
  scalaVersion := "2.11.6"
)

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    name := "chess-player",
    version := "0.1.0",
    scalacOptions ++= Seq("-feature", "-language:postfixOps")
  )
