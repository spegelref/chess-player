package se.spegelreflex.chess

/**
  * Game piece
  */
object GamePiece {
  def apply(piece: PieceType, color: Color): GamePiece =
    new GamePiece(piece match {
      case King => new King()
      case Queen => new Queen()
      case Rook => new Rook()
      case Bishop => new Bishop()
      case Knight => new Knight()
      case Pawn => new Pawn()
    }, color)
}

class GamePiece(val piece: Piece, val color: Color) {
  def movesFor(pos: Position, on: Board): List[Position] = piece match {
    case p: Pawn => p.movesFor(pos, color, on)
    case _ => piece movesFor pos
  }

  def toString(pos: Position): String =
    (piece toString) + (pos toString)

  override def toString(): String =
    color match {
      case Black => piece toString
      case White => (piece toString) toLowerCase
    }
}

/**
  * Piece code.
  */
abstract class Piece(val points: Int) {
  def movesFor(from: Position): List[Position]

  protected def inside(pos: Position): Boolean =
      (1 <= pos.x && pos.x <= 8) && (1 <= pos.y && pos.y <= 8)

  protected def move(from: Position)(pos: Position): Position =
    Position(from.x + pos.x, from.y + pos.y)

  protected def self(from: Position)(pos: Position): Boolean =
    (from.x == pos.x && from.y == pos.y)

  protected def line(): List[Position] =
    (-8 to 8).map(i => Position(0, i)).toList

  protected def lineInv(): List[Position] =
    (-8 to 8).map(i => Position(i, 0)).toList

  protected def diag(): List[Position] =
    (-8 to 8).map(i => Position(i, i)).toList

  protected def diagInv(): List[Position] =
    (-8 to 8).map(i => Position(i, -i)).toList
}

case class King() extends Piece(0) {
  override def movesFor(from: Position) =
    List(Position(from.x, from.y + 1), Position(from.x + 1, from.y + 1),
      Position(from.x + 1, from.y), Position(from.x + 1, from.y - 1),
      Position(from.x, from.y - 1), Position(from.x - 1, from.y - 1),
      Position(from.x - 1, from.y), Position(from.x - 1, from.y + 1))

  override def toString = "K"
}

case class Queen() extends Piece(9) {
  override def movesFor(from: Position) =
    (line ++ lineInv ++ diag ++ diagInv).map(move(from))
      .filter(inside).filterNot(self(from))

  override def toString = "Q"
}

case class Rook() extends Piece(3) {
  override def movesFor(from: Position) =
    (line ++ lineInv).map(move(from))
      .filter(inside).filterNot(self(from))

  override def toString = "R"
}

case class Bishop() extends Piece(5) {
  override def movesFor(from: Position): List[Position] =
    (diag ++ diagInv).map(move(from))
      .filter(inside).filterNot(self(from))

  override def toString = "B"

}

case class Knight() extends Piece(3) {
  override def movesFor(from: Position) =
    (mirrorX(from.x + 2, from.y) ++ mirrorX(from.x - 2, from.y) ++
      mirrorY(from.x, from.y + 2) ++ mirrorY(from.x, from.y - 2))
      .filter(inside)

  override def toString = "N"

  private def mirrorX(x: Int, y: Int): List[Position] =
    List(Position(x, y + 1), Position(x, y - 1))

  private def mirrorY(x: Int, y: Int): List[Position] =
    List(Position(x + 1, y), Position(x - 1, y))
}

case class Pawn() extends Piece(1) {
  def movesFor(from: Position, by: Color, board: Board): List[Position] =
    by match {
      case White => List(Position(from.x, from.y+1))
      case Black => List(Position(from.x, from.y-1))
    }

  override def movesFor(from: Position): List[Position] = {
    val possible: List[Position] =
      List(Position(from.x, from.y-1), Position(from.x, from.y+1))

    val firstMove =
      if (from.x == 2) Position(from.x, from.y + 2)
      else if (from.x == 7) Position(from.x, from.y - 2)
      else null

    if (firstMove == null) possible.filter(inside)
    else (firstMove :: possible).filter(inside)
  }

  override def toString = "P"
}

abstract class PieceType
case object King extends PieceType
case object Queen extends PieceType
case object Rook extends PieceType
case object Bishop extends PieceType
case object Knight extends PieceType
case object Pawn extends PieceType

/**
  */
abstract class Color
case object White extends Color
case object Black extends Color
