package se.spegelreflex.chess

object Board {
  val basicBoard = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"

  // Temporary
  def apply(fen: String = basicBoard) =
    new Board(initializeBoard(fen), White)

  private def initializeBoard(fen: String) = {
    Map(
      // White pieces
      Position(1,1) -> GamePiece(Rook, White),
      Position(1,2) -> GamePiece(Knight, White),
      Position(1,3) -> GamePiece(Bishop, White),
      Position(1,4) -> GamePiece(Queen, White),
      Position(1,5) -> GamePiece(King, White),
      Position(1,6) -> GamePiece(Bishop, White),
      Position(1,7) -> GamePiece(Knight, White),
      Position(1,8) -> GamePiece(Rook, White),
      Position(2,1) -> GamePiece(Pawn, White),
      Position(2,2) -> GamePiece(Pawn, White),
      Position(2,3) -> GamePiece(Pawn, White),
      Position(2,4) -> GamePiece(Pawn, White),
      Position(2,5) -> GamePiece(Pawn, White),
      Position(2,6) -> GamePiece(Pawn, White),
      Position(2,7) -> GamePiece(Pawn, White),
      Position(2,8) -> GamePiece(Pawn, White),
      // Black pieces.
      Position(7,1) -> GamePiece(Pawn, Black),
      Position(7,2) -> GamePiece(Pawn, Black),
      Position(7,3) -> GamePiece(Pawn, Black),
      Position(7,4) -> GamePiece(Pawn, Black),
      Position(7,5) -> GamePiece(Pawn, Black),
      Position(7,6) -> GamePiece(Pawn, Black),
      Position(7,7) -> GamePiece(Pawn, Black),
      Position(7,8) -> GamePiece(Pawn, Black),
      Position(8,1) -> GamePiece(Rook, Black),
      Position(8,2) -> GamePiece(Knight, Black),
      Position(8,3) -> GamePiece(Bishop, Black),
      Position(8,4) -> GamePiece(King, Black),
      Position(8,5) -> GamePiece(Queen, Black),
      Position(8,6) -> GamePiece(Bishop, Black),
      Position(8,7) -> GamePiece(Knight, Black),
      Position(8,8) -> GamePiece(Rook, Black)
    )
  }
}

class Board(current: Map[Position, GamePiece],
            nextTurn: Color) {
  def positions = current.keys.toList

  def occupied(pos: Position) = current.contains(pos)

  def pieceAt(pos: Position) = current.get(pos)

  override def toString: String = {
    def split(row: List[Option[GamePiece]]): (Int, List[Option[GamePiece]]) =
      (row.takeWhile(x => x.isEmpty).size, row.dropWhile(x => x.isEmpty))

    def printPiece(list: List[Option[Any]]): String =
      list.map({
        case Some(item) => item.toString
        case None => ""
      }).mkString("")

    def printNum(i: Int): String =
      if (i == 0) ""
      else i.toString

    def mkRowString(row: List[Option[GamePiece]], acc: String): String =
      if (row.isEmpty) acc
      else split(row) match {
        case (i, rest) => {
          val pieces = rest.takeWhile(x => !x.isEmpty)
          val empty = rest.dropWhile(x => !x.isEmpty)
          mkRowString(empty,
            acc + printNum(i) + printPiece(pieces))
        }
      }

    def getRow(x: Int): List[Option[GamePiece]] =
      (for (y <- 1 to 8) yield current.get(Position(x, y))).toList

    (for (x <- 1 to 8) yield mkRowString(getRow(x), "")).toList.mkString("/")
  }
}

object Position {
  def apply(x: Int, y: Int) = new Position(x, y)
  def unapply(pos: Position) = Some(pos.x, pos.y)
}

class Position(val x: Int, val y: Int) {
  override def toString = {
    def xAsChar = ('a' + x - 1).toChar

    s"${xAsChar}${y}"
  }

  override def equals(obj: Any): Boolean = {
    private def equals(pos: Position): Boolean =
      pos.x == this.x && pos.y == this.y

    obj.isInstanceOf[Position] && equals(obj.asInstanceOf[Position])
  }

  override def hashCode = (31 * (x + 31) + y)
}

class Player {
}
